package com.epam.model;

import com.epam.model.bank.*;

public class Main {
    private Integer bankNum;
    private Name name;
    private Country country;
    private Type type;
    private Depositor myDepositor;
    private AccountID accountID;
    private Profitability profitability;
    private AmountOnDeposit amountOnDeposit;
    private Constraints constraints;

    public Main(){
    }

    public Main(Integer bankNum, Name name, Country country, Type type, Depositor myDepositor, AccountID accountID, Profitability profitability, AmountOnDeposit amountOnDeposit, Constraints constraints) {
        this.bankNum = bankNum;
        this.name = name;
        this.country = country;
        this.type = type;
        this.myDepositor = myDepositor;
        this.accountID = accountID;
        this.profitability = profitability;
        this.amountOnDeposit = amountOnDeposit;
        this.constraints = constraints;
    }

    public Integer getBankNum() {
        return bankNum;
    }

    public void setBankNum(Integer bankNum) {
        this.bankNum = bankNum;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Depositor getMyDepositor() {
        return myDepositor;
    }

    public void setMyDepositor(Depositor myDepositor) {
        this.myDepositor = myDepositor;
    }

    public AccountID getAccountID() {
        return accountID;
    }

    public void setAccountID(AccountID accountID) {
        this.accountID = accountID;
    }

    public Profitability getProfitability() {
        return profitability;
    }

    public void setProfitability(Profitability profitability) {
        this.profitability = profitability;
    }

    public AmountOnDeposit getAmountOnDeposit() {
        return amountOnDeposit;
    }

    public void setAmountOnDeposit(AmountOnDeposit amountOnDeposit) {
        this.amountOnDeposit = amountOnDeposit;
    }

    public Constraints getConstraints() {
        return constraints;
    }

    public void setConstraints(Constraints constraints) {
        this.constraints = constraints;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "bankNum=" + bankNum +
                ", Name=" + name +
                ", Country=" + country +
                ", Type=" + type +
                ", Depositor=" + myDepositor +
                ", AccountID=" + accountID +
                ", Profitability=" + profitability +
                ", AmountOnDeposit=" + amountOnDeposit +
                ", Constraints=" + constraints +
                '}';
    }
}
