package com.epam.model.constants;

public class ConstantExeptions {
    public static final String IO_EXCEPTION
            = "Something wrong with file...\nIOException\n\n";

    public static final String VALIDATION_EXCEPTION
            = "Current document .json don't pass the tsts!\nValidationException\n\n";
}
