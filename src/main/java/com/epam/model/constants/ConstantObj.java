package com.epam.model.constants;

import com.epam.model.Main;


import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.lang.reflect.Type;
import java.util.List;


import static com.epam.model.constants.ConstantWays.WAY_BANK_JSON;

public class ConstantObj {
    public static final File FILE_JSON_BANK = new File(WAY_BANK_JSON);
    public static final String EMPTY_STRING = "";
    public static final String FUNCTION_START = "*****";
    public static final String FUNCTION_END = "*****";
    public static final Type TYPE = new TypeToken<List<Main>>() {
    }.getType();

    private ConstantObj() {
    }
}
