package com.epam.model.bank;

public class Constraints {
    private Double amount;
    private String type;

    public Constraints(){
    }

    public Constraints(Double amount, String type) {
        this.amount = amount;
        this.type = type;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Constraints{" +
                "amount=" + amount +
                ", type='" + type + '\'' +
                "}\n";
    }
}
