package com.epam;


import com.epam.parsers.jackson.RunJackson;

import java.util.Random;

import static com.epam.parsers.jackson.JacksonValidator.runMyValidator;

public class Main {
    public static void main(String[] args) {
        runMyValidator();
        task.controller.gson.RunGSON.run();
        RunJackson.run();
    }
}
