package task.controller.gson;

import com.epam.model.Main;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

import static com.epam.model.constants.ConstantObj.*;
import static com.epam.parsers.jackson.FileReader.readByBufferedReader;


public class RunGSON {
    private static final Logger LOG = LogManager.getLogger(RunGSON.class);
    private Gson gson;

    public RunGSON() {
        gson = new Gson();
    }

    List<Main> fromJsonReadBanks(String json) {
        LOG.debug(FUNCTION_START + "fromJsonReadBanksByGsonParser*****" + FUNCTION_END);
        return gson.fromJson(json, TYPE);
    }

    String toJsonGetString(Object obj) {
        LOG.debug(FUNCTION_START + "toJsonGetString" + FUNCTION_END);
        return gson.toJson(obj);
    }

        public static void run() {
            String json = readByBufferedReader(FILE_JSON_BANK, false);
            RunGSON gsonParser = new RunGSON();
            List<Main> banks = gsonParser.fromJsonReadBanks(json);
            String jsonAfterSetObject = gsonParser.toJsonGetString(banks);
            LOG.info(jsonAfterSetObject);
        }

}

