package com.epam.parsers.jackson;

import com.epam.model.Main;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.model.constants.ConstantObj.EMPTY_STRING;
import static com.epam.model.constants.ConstantObj.FUNCTION_END;
import static com.epam.model.constants.ConstantObj.FUNCTION_START;

public class JaksonParserItself {
    private static final Logger LOG = LogManager.getLogger(JaksonParserItself.class);
    private ObjectMapper mapper;

    JaksonParserItself(){
        mapper = new ObjectMapper();
    }

    String toJsonFromOjectToString(List<Main> banks){
        LOG.debug(FUNCTION_START+"readJsonFromStringToObject"+FUNCTION_END);

        String jsonText = EMPTY_STRING;
        try {
            jsonText = mapper.writeValueAsString(banks);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonText;
    }

    List<Main> fromJsonToObject(String jsonText){
        LOG.debug(FUNCTION_START+"fromJsonToObject"+FUNCTION_END);
        List<Main> banks = new ArrayList<>();
        try {
            banks = mapper.readValue(jsonText, List.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return banks;
    }
}
