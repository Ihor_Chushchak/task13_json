package com.epam.parsers.jackson;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;

import static com.epam.model.constants.ConstantExeptions.VALIDATION_EXCEPTION;
import static com.epam.model.constants.ConstantWays.WAY_BANK_JSON;
import static com.epam.model.constants.ConstantWays.WAY_BANK_JSON_SCHEMA;

public class JacksonValidator {
    private static final Logger LOG = LogManager.getLogger(System.in);
    public static void runMyValidator(){
        try {
            JSONObject jsonSchema = new JSONObject(WAY_BANK_JSON_SCHEMA);
            JSONObject jsonObject = new JSONObject(WAY_BANK_JSON);
            Schema schema = SchemaLoader.load(jsonSchema);
            schema.validate(jsonObject);
        }catch (Exception e){
            LOG.error(VALIDATION_EXCEPTION);
        }
    }
}
