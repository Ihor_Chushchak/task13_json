package com.epam.parsers.jackson;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

import static com.epam.model.constants.ConstantExeptions.IO_EXCEPTION;
import static com.epam.model.constants.ConstantObj.FUNCTION_END;
import static com.epam.model.constants.ConstantObj.FUNCTION_START;


public class FileReader {
    private static final Logger LOG = LogManager.getLogger(FileReader.class);

    private static void showHelper(boolean isShow, StringBuilder text) {
        if (isShow) {
            showString(text.toString());
        }
    }

    public static String readByBufferedReader(File file, boolean isShowFile) {
        LOG.debug(FUNCTION_START + "ReadByBufferedReader" + FUNCTION_END);
        StringBuilder textFile = new StringBuilder();
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(file)))) {
            while (br.ready()) {
                textFile.append(br.readLine());
            }
        } catch (IOException e) {
            LOG.error(IO_EXCEPTION);
            e.printStackTrace();
        }
        showHelper(isShowFile, textFile);
        return textFile.toString();
    }

        public static void showString(String textOutput) {
            LOG.info(textOutput);
        }

}
