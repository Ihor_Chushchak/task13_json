package com.epam.parsers.jackson;

import com.epam.model.Main;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

import static com.epam.model.constants.ConstantObj.FILE_JSON_BANK;
import static com.epam.parsers.jackson.FileReader.readByBufferedReader;

public class RunJackson {
    private static final Logger LOG = LogManager.getLogger(RunJackson.class);

    public static void run(){
        String json = readByBufferedReader(FILE_JSON_BANK,false);
        JaksonParserItself jacksonParser = new JaksonParserItself();
        List<Main> bankList = jacksonParser.fromJsonToObject(json);
        String jsonAfterReadJson = jacksonParser.toJsonFromOjectToString(bankList);
        LOG.info(jsonAfterReadJson);
    }
}
